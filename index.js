let favFood = "Flat Bread";
let sumNumbers = 150 + 9;
let prodNumbers = 100 * 90;
let isActive = true;
let favRestaurants = ["KFC", "McDonald's", "Jollibee", "Mang Inasal", "Chowking"];
let favSinger = {
	firstName: null,
	lastName: null,
	stageName: "E ve",
	birthday: "May 23, 1995",
	age: 26,
	bestAlbum: "Bunka",
	bestSong: "Furiwoshita",
	isActive: true
};

console.log(favFood);
console.log(sumNumbers);
console.log(prodNumbers);
console.log(isActive);
console.log(favRestaurants);
console.log(favSinger);

function division(x, y){
	return x / y;
}

let quotient = division(10, 5);

console.log("The result of the division is " + quotient);